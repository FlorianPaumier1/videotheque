import consumer from "./consumer";

consumer.subscriptions.create("RoomChannel", {
    connected(){
        let $element = $("[data-channel-subscribe=\"room\"]")
        $element.animate({ scrollTop: $element.prop("scrollHeight")}, 1000)
    },
    disconnected(){},
    received(data){

        let $element = $("[data-channel-subscribe=\"room\"]")
        let messageTemplate = $('[data-role="message-template"]');

        console.log(data)
        let content = messageTemplate.children().clone(true, true);

        content.find('[data-role="message-text"]').text(data.message);
        content.find('[data-role="message-date"]').text(data.date);
        content.find('[data-role="message-user"]').text(data.user);

        $element.append(content);
        console.log($element)
        $element.animate({ scrollTop: $element.prop("scrollHeight")}, 1000);
    }
})