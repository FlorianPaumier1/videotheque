json.extract! user_film, :id, :created_at, :updated_at
json.url user_film_url(user_film, format: :json)
