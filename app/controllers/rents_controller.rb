class RentsController < ApplicationController
  before_action :set_rent, only: [:show, :edit, :update, :destroy, :valid_rent]

  # GET /rents
  # GET /rents.json
  def index
    @rentss = Rent.all
    @rents = current_user.rents
    @ask = Rent.where.not(user_id: current_user.id)
               .joins(:films_user)
               .where("films_users.user_id = ?", current_user.id)
  end

  # GET /rents/1
  # GET /rents/1.json
  def show
  end

  def valid_rent

    @rent.valided = true
    @rent.valid_until = 10.days.from_now

    respond_to do |format|
      if @rent.update(rent_params)
        format.html { redirect_to @rent, notice: 'Rent was successfully validated.' }
        format.json { render :show, status: :ok, location: @rent }
      else
        format.html { render :edit }
        format.json { render json: @rent.errors, status: :unprocessable_entity }
      end
    end
  end


  # GET /rents/new
  def new
    @rent = Rent.new
    @list = FilmsUser.where.not(user: current_user)
  end

  # GET /rents/1/edit
  def edit
  end

  # POST /rents
  # POST /rents.json
  def create

    film = FilmsUser.find(params[:rent][:films_user_id])

    @rent = Rent.new
    @rent.user = current_user
    @rent.films_user=film
    @rent.valided = false
    @rent.created_at = Time.zone.now
    @rent.updated_at = Time.zone.now


    respond_to do |format|
      if @rent.save
        format.html { redirect_to rents_path, notice: 'Rent was successfully created.' }
        format.json { render :show, status: :created, location: @rent }
      else
        format.html { render :new }
        format.json { render json: @rent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rents/1
  # PATCH/PUT /rents/1.json
  def update
    if @rent.films_user.user != current_user
      respond_to do |format|
        format.html { redirect_to rents_path, error: "You can't update this rent"}
      end
    end
    respond_to do |format|
      if @rent.update(rent_params)
        format.html { redirect_to @rent, notice: 'Rent was successfully updated.' }
        format.json { render :show, status: :ok, location: @rent }
      else
        format.html { render :edit }
        format.json { render json: @rent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rents/1
  # DELETE /rents/1.json
  def destroy
    @rent.destroy
    respond_to do |format|
      format.html { redirect_to rents_url, notice: 'Rent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rent
      @rent = Rent.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def rent_params
      params.fetch(:rent, {})
    end
end
