module Admin
  class DashboardController < ApplicationController
    def index

      @users = User.where("role = 'ROLE_USER'").count
      @users_connections = User.where("role = 'ROLE_USER'").sum(:sign_in_count)
      @films = Film.count
      @rental_count = Rent.count
      @rental_validate = Rent.where("valided = true").count
      @rental_unvalidate = Rent.where("valided = false").count
      @contact_month = Contact.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
    end
  end
end