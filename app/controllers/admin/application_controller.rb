module Admin
  class ApplicationController < ::ApplicationController

    layout 'layouts/admin/base'

    before_action :isSigned


    def isSigned
      if !user_signed_in?
        redirect_to new_user_session_path
      end
    end
  end
end