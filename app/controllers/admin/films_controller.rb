module Admin
class FilmsController < ApplicationController
  before_action :set_film, only: [:show, :edit, :update, :destroy]

  # GET /films
  # GET /films.json
  def index
    page = params[:page].to_i
    @count = Film.all.count
    @start = (page - 5) * 15 < 0 ? 1 : page - 5
    @end = (page + 5) * 15 > @count ? @count / 15 : page + 5
    @films = Film.page(page).per(15)
  end

  # GET /films/1
  # GET /films/1.json
  def show
  end

  # GET /films/new
  def new
    @film = Film.new
  end

  # GET /films/1/edit
  def edit
  end

  # POST /films
  # POST /films.json
  def create
    @film = Film.new(film_params)

    respond_to do |format|
      if @film.save
        format.html { redirect_to @film, notice: 'Film was successfully created.' }
        format.json { render :show, status: :created, location: @film }
      else
        format.html { render :new }
        format.json { render json: @film.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /films/1
  # PATCH/PUT /films/1.json
  def update
    respond_to do |format|
      if @film.update(film_params)
        format.html { redirect_to @film, notice: 'Film was successfully updated.' }
        format.json { render :show, status: :ok, location: @film }
      else
        format.html { render :edit }
        format.json { render json: @film.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /films/1
  # DELETE /films/1.json
  def destroy
    @film.destroy
    respond_to do |format|
      format.html { redirect_to films_url, notice: 'Film was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
    if user_signed_in?
      respond_to do |format|
        if current_user.role == 'ROLE_ADMIN'
          require 'csv'
          table = CSV.parse(File.read("storage/movies_metadata.csv"), headers: true)

          # genres = row[3]
          table.each do |row|
            film = Film.find_by(:title => row[8])

            if film === nil
              Film.create(
                  :adult => row[0],
                  :imdb_id => row[6],
                  :genres => row[3],
                  :runtime => row[16],
                  :title => row[8],
                  :description => row[9],
                  :thumbnail => row[11],
                  :realese_date => row[14]
              )
            end
          end
        end

        format.html { redirect_to films_url, notice: 'Film was successfully imported.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_film
      @film = Film.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def film_params
      params.fetch(:film, {})
    end
end
end
