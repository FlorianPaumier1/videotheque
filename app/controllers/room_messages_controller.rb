class RoomMessagesController < ApplicationController
  before_action :authenticate_user!
  before_action :load_entities


  def create
    @room_message = RoomMessage.create user: current_user,
                                       room: @room,
                                       message: params.dig(:room_message, :message)

    if @room_message.save
      ActionCable.server.broadcast 'room_channel',
                                   message: @room_message.message,
                                   user: @room_message.user.username,
                                   date: @room_message.created_at
    end
  end

  protected

  def load_entities
    @room = Room.find params.dig(:room_message, :room_id)
  end
end
