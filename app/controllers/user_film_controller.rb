class UserFilmController < ApplicationController

  before_action :set_user_film, only: [:index,:add_list,:destroy]

  def index
  end

  def add_list
    film = Film.find(params[:id])

    if film != nil
      @list<<film
      notice = 'Film was not add.'
    else
      notice = 'Film was successfully delete.'
    end

    respond_to do |format|
      format.html { redirect_to films_path, notice: notice }
      format.json { head :no_content }
    end
  end

  def delete
    film = Film.find(params[:id])
    current_user.films.delete(film)
    respond_to do |format|
      format.html { redirect_to user_film_index, notice: 'Film was successfully delete.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user_film
    @list = current_user.films
  end


end