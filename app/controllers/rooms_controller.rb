class RoomsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_room, only: [:show]

  def index
    @rooms = Room.all.joins(:users).where('users.id = ?', current_user.id)
  end

  def show
    @room_message = RoomMessage.new room: @room
    @room_messages = @room.room_messages.includes(:user)
  end

  def new
    @room = Room.new
  end

  def create
    @room = Room.new

    @room.name = room_params[:name]
    @room.users<<current_user

    room_params[:users].each do |id|
      @room.users<<User.find(id)
    end

    respond_to do |format|
      if @room.save
        format.html { redirect_to rooms_path, notice: 'Room was successfully created.' }
        format.json { render :show, status: :created, location: rooms_path }
      else
        format.html { render :new }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end


  private

  def set_room
    @room = Room.find(params[:id])
  end

  def room_params
    params.require(:room).permit(:name, users: [])
  end
end
