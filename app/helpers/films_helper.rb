module FilmsHelper

  def notInList(id)
    if user_signed_in?
      inList = current_user.films.exists?(id)

      return !inList
    end
  end

end
