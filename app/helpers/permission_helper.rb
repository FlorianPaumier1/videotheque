module PermissionHelper

  def isSignedIn?
    user_signed_in?
  end

  def isAdmin?
    user_signed_in? && current_user.role === "ROLE_ADMIN"
  end

end