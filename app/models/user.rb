class User < ApplicationRecord

  attr_accessor :login

  has_many :rents
  has_many :room_users
  has_many :rooms, through: :room_users
  has_many :room_messages

  has_many :films_users
  has_many :films , through: :films_users

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,:confirmable, :lockable, :timeoutable, :trackable

  validates :username, presence: true, uniqueness: {case_sensitive: false}, format: {with: /\A[a-zA-Z0-9 _\.]*\z/}

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup

    if login = conditions.delete(:login)
      where(conditions.to_hash).where("lower(username) = :value or lower(email) = :value", value: login.downcase).first
    else
      where(conditions.to_hash).first
    end
  end
end
