class Film < ApplicationRecord

  has_many :films_users

  has_many :users, through: :films_users

end