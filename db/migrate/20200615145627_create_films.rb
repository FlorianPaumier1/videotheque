class CreateFilms < ActiveRecord::Migration[6.0]
  def change
    create_table :films do |t|
      t.string :title
      t.boolean :adult
      t.integer :budget
      t.json :genres
      t.string :imdb_id
      t.text :description
      t.string :thumbnail
      t.datetime :realese_date
      t.float :runtime
    end
    add_index :films, :title, unique: true
  end
end
