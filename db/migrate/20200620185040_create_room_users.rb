class CreateRoomUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :room_users do |t|
      t.belongs_to :user
      t.belongs_to :room
      t.boolean :authorize
    end
  end
end
