class CreateRents < ActiveRecord::Migration[6.0]
  def change
    create_table :rents do |t|
      t.datetime :valid_until
      t.boolean :valided

      t.belongs_to :user
      t.belongs_to :films_user

      t.timestamps
    end
  end
end
