class CreateJoinTableFilmsUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :films_users do |t|
      t.belongs_to :user
      t.belongs_to :film
    end
  end
end
