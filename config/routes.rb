Rails.application.routes.draw do

  resources :room_users
  resources :room_messages
  resources :rooms

  root "home#index"


  devise_for :users

  resources :rents
  get '/rents/validation/:id', to: 'rents#valid_rent', as: 'valid_rent'

  resources :films
  get '/films/import', to: 'films#import', as: 'import_film'
  get '/film/add/:id', to: 'user_film#add_list', as: 'add_user_film'

  resources :contacts
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html



  scope '/user-film' do
    get '/', to: "user_film#index", as: 'user_film_index'
    delete '/:id', to: "user_film#delete", as: 'user_film_delete'
  end


  scope 'admin', module: 'admin', as: 'admin' do
    get '/dashboard', to: 'dashboard#index', as: 'dashboard'
    resources :users
    resources :rents
    resources :contacts
    resources :films
  end
end
